const fs = require('fs');

function loadjson(filename) {
	const rawdata = fs.readFileSync(filename);
	return JSON.parse(rawdata);
}

function forEachInDir(dirname, fn) {
	const dir = fs.opendirSync(dirname)
	let dirent
	while ((dirent = dir.readSync()) !== null) {
		fn(dirent.name, dirname);
	}
	dir.closeSync()
}


function loadInputRules() {
	const rules = [];
	
	forEachInDir('C:/Users/David/git-work/dataintegration/usr/rules/', (filename, dirname) => {
		const rulejson = loadjson(dirname + filename);
		rules.push(rulejson);
	});
	
	return rules;
}

function urlsafe(str) {
	return str.replace(/[^a-z0-9]/gi, '_');
}


const rules = loadInputRules();

const COMP_TAGS = ['CIS', 'NIST 800-53', 'NIST 800-171', 'FedRAMP', 'HIPAA', 'PCI DSS', 'GDPR', 'AWS Well-Architected (Security)', 'AWS Well-Architected (Reliability)'];

const output = {};

for (const r of rules) {
	const cloudName = (r.search.entityTypeFilters.map(f => f.datasource)[0] || '').replace('Amazon', 'AWS');
	const serviceName = r.search.entityTypeFilters.map(f => f.entityType).sort().join(' + ');
	const servicePath = urlsafe(serviceName);
	const rulePath = urlsafe(r.name);
	
	r.cloud = cloudName;
	r.serviceName = serviceName;
	r.servicePath = servicePath;
	r.rulePath = rulePath;
	
	// separate description
	const descParts = r.description ? r.description.split('\n\n') : [];
	r.shortDescription = descParts[0] || '';
	r.detailedDescription = descParts.length === 3 ? descParts[1] : '';
	r.complianceDescription = descParts[descParts.length-1] || '';
	r.preferredDescription = r.detailedDescription !== '' ? r.detailedDescription : r.shortDescription;
	r.descriptionText = r.detailedDescription !== '' ? r.shortDescription + '\n\n' + r.detailedDescription : r.shortDescription;
	
	// separate compliance tags
	r.complianceTags = COMP_TAGS.filter(t => r.tags.includes(t));
	
	r.kind = [];
	if (r.tags.includes("Security") || r.tags.includes("Compliance")) {
		r.kind.push("Security | Compliance");
	}
	if (r.tags.includes("Cost")) {
		r.kind.push("Cost");
	}
	
	let cloud = output[cloudName];
	if (!cloud) {
		cloud = {};
		output[cloudName] = cloud;
	}
	
	let service = cloud[servicePath];
	if (!service) {
		service = {
			name: serviceName,
			rules: {}
		};
		cloud[servicePath] = service;
	}
	
	service.rules[rulePath] = r;
}

// sort by service
for (const [cloud, services] of Object.entries(output)) {
	const servicesOrdered = Object.keys(services).sort().reduce(
		  (obj, key) => { 
			obj[key] = services[key]; 
			return obj;
		  }, 
		  {}
		);
	
	output[cloud] = servicesOrdered;
}

console.log(JSON.stringify(output));